import csv
import os
import time
from io import TextIOWrapper
from datetime import datetime

from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy

from DBCreateScript import db_object
import db_insert
import db_select
import db_delete
import read_file

import TestExecution.WebAppRequest as web
import TestExecution.WinAppRequest as desktop

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = db_object.connection()
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

@app.route('/')
def test():
    return render_template('post.html')

@app.route('/<version>/file/register', methods=['POST'])
def register_file(version):
    pt_li = []
    f = TextIOWrapper(request.files['file'], encoding='utf-8-sig')
    tc_instance_list = read_file.parsing(f)
    db_insert.insert_all(tc_instance_list)

    project = db_select.PT_select("all")
    for i in range(len(project)):
        pt_li.append({'PT_number':project[i].PT_number, 'PT_name':project[i].PT_name})
    return jsonify(result="success", result2=pt_li)

@app.route('/<version>/project/registered', methods=['GET'])
def registered_PT(version):
    pt_li = []
    reply = request.args.get('success')
    if(reply=="success"):
        project = db_select.PT_select("all")
        for i in range(len(project)):
            pt_li.append({'PT_number':project[i].PT_number, 'PT_name':project[i].PT_name})
    return jsonify(result="success", result2=pt_li)

@app.route('/<version>/project/delete', methods=['DELETE'])
def delete_PT(version):
    reply = request.get_json()['success']
    if(reply=="success"):
        db_delete.delete_all()
    return jsonify(result="success")

@app.route('/<version>/project/select', methods=['GET'])
def select_PT(version):
    tp_li = []
    selected_pt = request.args.get('selected_pt')
    project = db_select.PT_select(db_object.Project.PT_number == selected_pt)
    platform = project[0].Platform
    testprocedure = db_select.TP_select(db_object.TestProcedure.PT_number == selected_pt)
    for i in range(len(testprocedure)):
        tp_li.append({'TP_number':testprocedure[i].TP_number, 'TP_name':testprocedure[i].TP_name})
    return jsonify(result="success", result2=tp_li, platform=platform)

@app.route('/<version>/testprocedure/select', methods=['GET'])
def select_TP(version):
    tc_li = []
    selected_tp = request.args.get('selected_tp')
    testcase = db_select.TC_select(db_object.TestCase.TP_number == selected_tp)
    for i in range(len(testcase)):
        tc_li.append({'TC_number':testcase[i].TC_number, 'TC_name':testcase[i].TC_name})
    return jsonify(result="success", result2= tc_li)

@app.route('/<version>/testcase/select', methods=['GET'])
def select_TC(version):
    ts_li = []
    selected_tc = request.args.get('selected_tc')
    teststep = db_select.TS_select(db_object.TestStep.TC_number == selected_tc)
    for i in range(len(teststep)):
        ts_li.append({'TS_number':teststep[i].TS_number, 'TS_info':teststep[i].TS_info, 'ER_info':teststep[i].ER_info, 'ER_value':teststep[i].ER_value})
    return jsonify(result="success", result2=ts_li)

@app.route('/<version>/web/firstExecution/update', methods=['PUT'])
def update_FE_web(version):  # Web Recording
    url = 'http://localhost:8089/'
    data = request.get_json()
    selected_tc = data['selected_tc']
    testcase = db_select.TC_select(db_object.TestCase.TC_number == selected_tc)
    teststep = db_select.TS_select(db_object.TestStep.TC_number == selected_tc)
    last_step = len(teststep) - 1
    testcase_name = testcase[0].TC_name.replace(" ", "")

    # Recording 실행
    web.recordAndSaveTestCase(testcase_name, url)

    recording_file_path = 'TestExecution/SELENIUM/'+testcase_name+'.py'
    recording_file_exist = os.path.isfile(recording_file_path)

    if(recording_file_exist): # recording file이 존재할 시
        teststep[last_step].FE_value = recording_file_path
    else:
        print("recording file이 존재하지 않습니다!")
    db_object.db.session.commit()

    return jsonify(result="success")

@app.route('/<version>/web/testExecution/update', methods=['PUT'])
def update_AR_web(version): # Web Test
    data = request.get_json()
    selected_tc = data['selected_tc']
    testcase = db_select.TC_select(db_object.TestCase.TC_number == selected_tc)
    teststep = db_select.TS_select(db_object.TestStep.TC_number == selected_tc)
    testcase_name = testcase[0].TC_name.replace(" ", "") # 실제 결과 이미지의 이름으로 사용
    last_step = len(teststep)-1
    start_datetime = datetime.now()
    start = time.time()
    # Web Test 실행
    web.runTest(testcase_name) # Test 후에 정해진 경로에 실제 결과 이미지인 'testcase_name.PNG'가 저장
    spend_time = time.time() - start
    print("test time : ",spend_time)

    image_path = 'static/actualImg/'+testcase_name+'.PNG'
    image_exist = os.path.isfile(image_path)
    if(image_exist): # 실제 결과 이미지가 존재할 시
        teststep[last_step].AR_value = image_path # 실제 결과 이미지 경로를 update
        teststep[last_step].Time = str(start_datetime)+'_'+str(round(spend_time,2))+'s'# test 시에 걸린 시간을 update
    else:
        print("실제 결과 이미지가 존재하지 않습니다!")
    db_object.db.session.commit()

    return jsonify(result="success", actualImg=teststep[last_step].AR_value)

@app.route('/<version>/web/testExecution/PF', methods=['PUT'])
def reporting_web_pf(version):
    data = request.get_json()
    selected_tc = data['selected_tc']
    PF = data['PF']
    testcase = db_select.TC_select(db_object.TestCase.TC_number == selected_tc)
    teststep = db_select.TS_select(db_object.TestStep.TC_number == selected_tc)
    testcase_name = testcase[0].TC_name.replace(" ", "")
    last_step = len(teststep)-1
    time = teststep[last_step].Time.split("_")
    reporting_data = [time[0], teststep[last_step].AR_value, time[1], PF]
    save_web_report('reporting/' + testcase_name + '.csv', reporting_data)
    return jsonify(result="success")

# 예상 결과가 image일 때 P/F를 전달 받으면 실제 결과, P/F, spend_time을 reporting
def save_web_report(path, data):
    file_exist = os.path.isfile(path)
    f = open(path,'a+',newline='')
    wr = csv.writer(f)
    if(not(file_exist)):
        wr.writerow(['DateTime', 'ActualResult', 'SpendingTime', 'PF'])
    wr.writerow([data[0], data[1], data[2], data[3]])
    f.close()

@app.route('/<version>/desktop/firstExecution/update', methods=['PUT']) # desktop recording
def update_FE_desktop(version): # DeskTop Recording
    data = request.get_json()
    selected_tc = data['selected_tc']
    testcase = db_select.TC_select(db_object.TestCase.TC_number == selected_tc)
    teststep = db_select.TS_select(db_object.TestStep.TC_number == selected_tc)
    testcase_name = testcase[0].TC_name.replace(" ", "")
    # desktop recording
    for i in range(len(teststep)):
        # desktop recording
        desktop.recordTest()
        recording_file_path = desktop.saveTestWithCaseAndStep(testcase_name,str(i+1))
        print("desktop: ", recording_file_path)
        recording_file_exist = os.path.isfile(recording_file_path)
        if(recording_file_exist):
            teststep[i].FE_value = recording_file_path
        else:
            print("recording file이 존재하지 않습니다!")
    db_object.db.session.commit()
    return jsonify(result="success")

@app.route('/<version>/desktop/afterExecution/reRecording', methods=['PUT']) # desktop recording
def reRecording_desktop(version): # DeskTop reRecording
    data = request.get_json()
    selected_tc = data['selected_tc']
    selected_ts = data['selected_ts']
    testcase = db_select.TC_select(db_object.TestCase.TC_number == selected_tc)
    teststep = db_select.TS_select(db_object.TestStep.TC_number == selected_tc)
    testcase_name = testcase[0].TC_name.replace(" ", "")
    # desktop reRecording
    desktop.recordTest()
    recording_file_path = desktop.saveTestWithCaseAndStep(testcase_name,selected_ts)
    print("desktop: ", recording_file_path)
    recording_file_exist = os.path.isfile(recording_file_path)
    if(recording_file_exist):
        print(int(selected_ts)-1)
        teststep[int(selected_ts)-1].FE_value = recording_file_path
    else:
        print("recording file이 존재하지 않습니다!")
    db_object.db.session.commit()
    return jsonify(result="success")

@app.route('/<version>/desktop/testExecution/update', methods=['PUT'])
def update_AR_desktop(version): # DeskTop Test
    data = request.get_json()
    selected_tc = data['selected_tc']
    testcase = db_select.TC_select(db_object.TestCase.TC_number == selected_tc)
    teststep = db_select.TS_select(db_object.TestStep.TC_number == selected_tc)
    testcase_name = testcase[0].TC_name.replace(" ", "")

    TS_actualImg = []
    for i in range(len(teststep)):
        start_datetime = datetime.now()
        start = time.time()
        # DeskTop Test 실행
        desktop.playbackTestWithCaseAndStep(testcase_name, str(i+1))
        desktop.captureScreenWithCaseAndStep(testcase_name, str(i+1))
        spend_time = time.time() - start
        image_path = 'static/actualImg/'+testcase_name+'_'+str(i+1)+'.PNG'
        image_exist = os.path.isfile(image_path)
        if(image_exist): # 실제 결과 이미지가 존재할 시
            teststep[i].AR_value = image_path  # 실제 결과 이미지 경로를 update
            TS_actualImg.append(teststep[i].AR_value)
            teststep[i].Time = str(start_datetime)+'_' +str(round(spend_time, 2))+'s'  # test 시에 걸린 시간을 update
        else:
            print("실제 결과 이미지가 존재하지 않습니다!")
    db_object.db.session.commit()
    return jsonify(result="success", actualImg=TS_actualImg)

@app.route('/<version>/desktop/afterExecution/reTest', methods=['PUT'])
def reTest_desktop(version): # DeskTop reTest
    data = request.get_json()
    selected_tc = data['selected_tc']
    selected_ts = data['selected_ts']
    testcase = db_select.TC_select(db_object.TestCase.TC_number == selected_tc)
    teststep = db_select.TS_select(db_object.TestStep.TC_number == selected_tc)
    testcase_name = testcase[0].TC_name.replace(" ", "")

    start_datetime = datetime.now()
    start = time.time()
    # DeskTop reTest 실행
    desktop.playbackTestWithCaseAndStep(testcase_name, selected_ts)
    desktop.captureScreenWithCaseAndStep(testcase_name, selected_ts)
    spend_time = time.time() - start
    image_path = 'static/actualImg/'+testcase_name+'_'+selected_ts+'.PNG'
    image_exist = os.path.isfile(image_path)
    if(image_exist): # 실제 결과 이미지가 존재할 시
        teststep[int(selected_ts)-1].AR_value = image_path  # 실제 결과 이미지 경로를 update
        TS_actualImg = teststep[int(selected_ts)-1].AR_value
        teststep[int(selected_ts)-1].Time = str(start_datetime)+'_' +str(round(spend_time, 2))+'s'  # test 시에 걸린 시간을 update
    else:
        print("실제 결과 이미지가 존재하지 않습니다!")
    db_object.db.session.commit()
    return jsonify(result="success", actualImg=TS_actualImg)

@app.route('/<version>/desktop/testExecution/PF', methods=['PUT'])
def reporting_desktop(version):
    # F/E에서 selected_tc(TC_number), PF 전달 필요
    data = request.get_json()
    selected_tc = data['selected_tc']
    PF = data['PF'] # list 형태
    testcase = db_select.TC_select(db_object.TestCase.TC_number == selected_tc)
    teststep = db_select.TS_select(db_object.TestStep.TC_number == selected_tc)
    testcase_name = testcase[0].TC_name.replace(" ", "")
    reporting_data = []
    for i in range(len(teststep)):
        time = teststep[i].Time.split("_")
        reporting_data.append([str(i+1), time[0], teststep[i].AR_value, time[1], PF[i]])
    save_desktop_report('reporting/' + testcase_name + '.csv', reporting_data)
    return jsonify(result="success")


# 예상 결과가 image일 때 P/F를 전달 받으면 실제 결과, P/F, spend_time을 reporting
def save_desktop_report(path, data):
    file_exist = os.path.isfile(path)
    f = open(path,'a+',newline='')
    wr = csv.writer(f)
    if(not(file_exist)):
        wr.writerow(['TestStep', 'DateTime', 'ActualResult', 'SpendingTime', 'PF'])
    final_PF = 'Pass'
    for i in range(len(data)):
        if(data[i][4]=='Fail'):
            final_PF = 'Fail'
        wr.writerow([data[i][0], data[i][1], data[i][2], data[i][3], data[i][4]])
    # 최종 Pass/Fail 결정
    wr.writerow(['최종','','','',final_PF])
    f.close()

if __name__ == '__main__':
    db_object.create_table()
    app.run(debug=False)













