from flask import Flask, render_template, request
from flask_api import status
import json
from TestExecution.WinAppRequest import *

app = Flask(__name__)

'''
Description : Record and Playback Usage from Flask
'''


@app.route('/playback', methods=['POST'])
def playbackProcess():
    '''
    Parse Request
    '''
    data = request.get_data().decode('utf-8')
    js_data = json.loads(data)
    testcase_name = js_data['testcasename']

    '''
    Get Data from DB ...
    '''
    teststep_count_from_db = getTestList()[testcase_name]
    actual_value_location_from_db = range(1,teststep_count_from_db+1)

    '''
    Playback and Get Result
    '''
    result = runTestStepsWithFindResult(testcase_name, teststep_count_from_db, list(actual_value_location_from_db))
    return {'msg' : "[LOG] Success : Playback  " + testcase_name, 'data': json.dumps(result)}, status.HTTP_200_OK


@app.route('/record', methods=['POST'])
def recordProcess():
    '''
    Parse Request
    '''
    data = request.get_data().decode('utf-8')
    js_data = json.loads(data)
    testcase_name = js_data['testcasename']
    step_count = js_data['stepcount']

    '''
    Record and Get Result
    '''
    result = recordAndSaveTestSteps(testcase_name, step_count)
    return {'msg' : "[LOG] Success : Recording  " + testcase_name, 'data': json.dumps(result)}, status.HTTP_200_OK


@app.route('/')
def test():
    filelist = getTestList()
    return render_template('request_rnp.html', testlist=filelist)


if __name__ == '__main__':
    app.run(debug=False)













