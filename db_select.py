from DBCreateScript import db_object
from sqlalchemy import func

def PT_select(selectData):
    if (selectData=="all"):
        PT_data = db_object.db.session.query(db_object.Project).filter().all()
    elif (selectData=="count"):
        PT_data = db_object.db.session.query(func.count(db_object.Project.PT_number))
    else:
        PT_data = db_object.db.session.query(db_object.Project).filter(selectData).all()
    return PT_data

def TP_select(selectData):
    if (selectData=="count"):
        TP_data = db_object.db.session.query(func.count(db_object.TestProcedure.TP_number))
    else:
        TP_data = db_object.db.session.query(db_object.TestProcedure).filter(selectData).all()
    return TP_data

def TC_select(selectData):
    if (selectData=="count"):
        TC_data = db_object.db.session.query(func.count(db_object.TestCase.TC_number))
    else:
        TC_data = db_object.db.session.query(db_object.TestCase).filter(selectData).all()
    return TC_data

def TS_select(selectData):
    TS_data =  db_object.db.session.query(db_object.TestStep).filter(selectData).all()
    return TS_data