from DBCreateScript import db_object

def delete_all():
    db_object.db.session.query(db_object.TestStep).delete()
    db_object.db.session.query(db_object.TestCase).delete()
    db_object.db.session.query(db_object.TestProcedure).delete()
    db_object.db.session.query(db_object.Project).delete()
    db_object.db.session.commit()