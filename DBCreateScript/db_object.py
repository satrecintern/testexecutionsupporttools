import yaml
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text

app = Flask(__name__)

def connection():
    with open('./mysql_connection.yaml') as f:
        conn = yaml.load(f, Loader=yaml.SafeLoader)
    return conn['url']

app.config['SQLALCHEMY_DATABASE_URI'] = connection()
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

class Project(db.Model):
    __tablename__ = "Project"
    column = ['PT_number', 'PT_name', 'Platform']
    PT_number = db.Column(db.Integer, primary_key=True, server_default=text("0"))
    PT_name = db.Column(db.String(50), nullable=False, server_default='noName')
    Platform = db.Column(db.String(20), nullable=False, server_default = 'noPlatform')
    def __init__(self, PTnumber, PTname, PF):
        self.PT_number = PTnumber
        self.PT_name = PTname
        self.Platform = PF
    testprocedure = db.relationship("TestProcedure", cascade="all, delete")

class TestProcedure(db.Model):
    __tablename__ = "TestProcedure"
    column = ['TP_number', 'PT_number', 'TP_name', 'TP_version']
    TP_number = db.Column(db.Integer, primary_key=True, server_default=text("0"))
    PT_number = db.Column(db.Integer, db.ForeignKey('Project.PT_number'), primary_key=True, server_default=text("0"))
    TP_name = db.Column(db.String(50), nullable=False, server_default='noName')
    TP_version = db.Column(db.String(50), nullable=False, server_default='0.0')
    def __init__(self, TPnumber, PTnumber, TPname, TPversion):
        self.TP_number = TPnumber
        self.PT_number = PTnumber
        self.TP_name = TPname
        self.TP_version = TPversion
    testcase = db.relationship("TestCase", cascade="all, delete")

class TestCase(db.Model):
    __tablename__ = "TestCase"
    column = ['TC_number', 'TP_number', 'TC_name', 'TC_info', 'PC_exist_or_not', 'PC_info', 'AR_info']
    TC_number = db.Column(db.Integer, primary_key=True, server_default=text("0"))
    TP_number = db.Column(db.Integer, db.ForeignKey('TestProcedure.TP_number'), primary_key=True, server_default=text("0"))
    TC_name = db.Column(db.String(50), nullable=False, server_default='noName')
    TC_info = db.Column(db.String(1000), nullable=False, server_default='noInfo')
    PC_exist_or_not = db.Column(db.Integer, nullable=False, server_default=text("0")) # exist = 1, not = 0
    PC_info = db.Column(db.String(1000), nullable=False, server_default='noInfo')
    def __init__(self, TCnumber, TPnumber, TCname, TCinfo, PCexist, PCinfo):
        self.TC_number = TCnumber
        self.TP_number = TPnumber
        self.TC_name = TCname
        self.TC_info = TCinfo
        self.PC_exist_or_not = PCexist
        self.PC_info = PCinfo
    teststep = db.relationship("TestStep", cascade="all, delete")

class TestStep(db.Model):
    __tablename__ = "TestStep"
    column = ['TC_number', 'TS_number', 'TS_info', 'ER_info', 'FE_info', 'AR_info']
    TC_number = db.Column(db.Integer, db.ForeignKey('TestCase.TC_number'), primary_key=True, server_default=text("0"))
    TS_number = db.Column(db.Integer, primary_key=True, server_default=text("0"))
    TS_info = db.Column(db.String(1000), nullable=False, server_default='noInfo')
    ER_info = db.Column(db.String(1000), nullable=False, server_default='noInfo') # 예상 결과 설명
    ER_value = db.Column(db.String(1000), nullable=False, server_default='noValue') # 예상 결과값
    FE_value = db.Column(db.String(1000), nullable=False, server_default='noValue') #초기 수행 결과값
    AR_value = db.Column(db.String(1000), nullable=False, server_default='noValue') #테스트 수행 결과값
    Time = db.Column(db.String(100), nullable=False, server_default='noTime')
    def __init__(self, TCnumber, TSnumber, TSinfo, ERinfo, ERvalue, FEvalue , ARvalue, Time):
        self.TC_number = TCnumber
        self.TS_number = TSnumber
        self.TS_info = TSinfo
        self.ER_info = ERinfo
        self.ER_value = ERvalue
        self.FE_value = FEvalue
        self.AR_value = ARvalue
        self.Time = Time

def create_table():
    db.create_all()
    db.session.commit()