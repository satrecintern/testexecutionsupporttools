import numpy as np

from DBCreateScript import db_object
import db_select

def PT_insert(tc_instance_list):
    for i in range(len(tc_instance_list)):
        exist = db_select.PT_select(db_object.Project.PT_name==tc_instance_list[i]._projectName)
        PT_data_num = db_select.PT_select("count")
        if(len(exist)==0): # project table에 중복된 데이터 없음
            PT_number = PT_data_num[0][0]+1
            PT_name = tc_instance_list[i]._projectName
            PF = tc_instance_list[i]._platform
            pt = db_object.Project(PT_number, PT_name, PF)
            db_object.db.session.add(pt)
        db_object.db.session.commit()

def TP_insert(tc_instance_list):
    for i in range(len(tc_instance_list)):
        exist = db_select.TP_select(db_object.TestProcedure.TP_name == tc_instance_list[i]._tpName)
        TP_data_num = db_select.TP_select("count")
        if(len(exist)==0): # testprocedure table에 중복된 데이터 없음
            TP_number = TP_data_num[0][0] + 1
            pt = db_select.PT_select(db_object.Project.PT_name == tc_instance_list[i]._projectName)
            PT_number = pt[0].PT_number
            version = tc_instance_list[i]._tpVersion
            if(type(version)==np.float64 or type(version)!=str): # type(nan) is float
                version = '0.0'
            tp = db_object.TestProcedure(TP_number, PT_number, tc_instance_list[i]._tpName, version)
            db_object.db.session.add(tp)
        db_object.db.session.commit()

def TC_insert(tc_instance_list):
    for i in range(len(tc_instance_list)):
        TC_data_num = db_select.TC_select("count")
        TC_number = TC_data_num[0][0]+1
        TP_data = db_select.TP_select(db_object.TestProcedure.TP_name == tc_instance_list[i]._tpName)
        TP_number = TP_data[0].TP_number
        des = tc_instance_list[i]._tcDes
        if(type(des)==float): # type(nan) is float
            des = 'noInfo'
        pc = tc_instance_list[i]._tcPrecon
        if(type(pc)==str and len(pc)>0): # type(nan) is float
            pc_exist = 1
        else:
            pc_exist = 0
            pc = 'noInfo'

        tc = db_object.TestCase(TC_number, TP_number, tc_instance_list[i]._tcName, des, pc_exist, pc)
        db_object.db.session.add(tc)
        db_object.db.session.commit()

def TS_insert(tc_instance_list):
    for i in range(len(tc_instance_list)):
        TC_data = db_select.TC_select(db_object.TestCase.TC_name == tc_instance_list[i]._tcName)
        TC_number = TC_data[0].TC_number
        ts_num = tc_instance_list[i]._stepId
        ts_info = tc_instance_list[i]._stepDes
        er_info = tc_instance_list[i]._expectedResultInfo
        er_value = tc_instance_list[i]._expectedResult
        #ar_value = tc_instance_list[i]._actualResult
        for j in range(len(ts_num)):
            eri = 'noInfo'
            if (type(er_info[j]) == str and len(er_info[j]) > 0):  # type(Nan) is float
                eri = er_info[j]
            erv = 'noValue'
            if (type(er_value[j]) == str and len(er_value[j]) > 0):  # type(Nan) is float
                erv = er_value[j]
            fev = 'noValue'
            arv = 'noValue'
            time = 'noTime'
            ts = db_object.TestStep(TC_number, int(ts_num[j]), ts_info[j], eri, erv, fev, arv, time)
            db_object.db.session.add(ts)
        db_object.db.session.commit()

def insert_all(tc_instance_list):
    PT_insert(tc_instance_list)
    TP_insert(tc_instance_list)
    TC_insert(tc_instance_list)
    TS_insert(tc_instance_list)
