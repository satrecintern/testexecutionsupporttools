var pt_combo = document.getElementById("project");
var tp_combo = document.getElementById("testprocedure");
var tc_combo = document.getElementById("testcase");
var pt_list;
var tp_list;
var tc_list;
var ts_list;
var category;
var ts_list;
var number_of_fail = 0;

 $('#file_btn').click(function(){
   var sendingData = new FormData();
   sendingData.append('file',document.getElementById('csv_file').files[0]);
   $('#project').children().nextAll().remove();
   $('#testprocedure').children().nextAll().remove();
   $('#testcase').children().nextAll().remove();
   $('#olist').children().remove();
   $('#webTestResult').children().remove();
   $.ajax({
     type: 'POST',
     enctype: 'multipart/form-data',
     url: '/v1.0/file/register',
     data: sendingData,
     contentType : false,
     processData : false,
     success: function(data){
       pt_list = data.result2;
       for(var i=0; i<pt_list.length; i++){
         var op = new Option();
         op.value = pt_list[i]['PT_number'];
         op.text = pt_list[i]['PT_name'];
         pt_combo.add(op);
     }
     },
     error: function(request, status, error){
       alert(request.status+" "+request.statusText);
       //alert(request.responseText);
     }
   })
 })

 $('#registered_btn').click(function(){
   console.log();
   $('#project').children().nextAll().remove();
   $('#testprocedure').children().nextAll().remove();
   $('#testcase').children().nextAll().remove();
   $('#olist').children().remove();
   var data = {
     'success': "success"
   }
   $.ajax({
     type: 'GET',
     url: '/v1.0/project/registered',
     data: data,
     success: function(data){
       pt_list = data.result2;
       for(var i=0; i<pt_list.length; i++){
         var op = new Option();
         op.value = pt_list[i]['PT_number'];
         op.text = pt_list[i]['PT_name'];
         pt_combo.add(op);
       }
     },
     error: function(request, status, error){
       alert(request.status+" "+request.statusText);
       //alert(request.responseText);
     }
   })
 })

 $('#delete_btn').click(function(){
   $('#project').children().nextAll().remove();
   $('#testprocedure').children().nextAll().remove();
   $('#testcase').children().nextAll().remove();
   $('#olist').children().remove();
   $('#webTestResult').children().remove();

   var data = {
     'success': "success"
   }
   $.ajax({
     type: 'DELETE',
     url: '/v1.0/project/delete',
     data: JSON.stringify(data),
     dataType : 'JSON',
     contentType: "application/json",
     success: function(data){
       console.log('success');


     },
     error: function(request, status, error){
       alert(request.status+" "+request.statusText);
       //alert(request.responseText);
     }
   })
 })

 $('#pt_btn').click(function(){
   var selected_pt = $('#project').val();
   $('#testprocedure').children().nextAll().remove();
   $('#testcase').children().nextAll().remove();
   $('#olist').children().remove();
   $('#divTestButton').empty();
   $('#webTestResult').children().remove();
   var data = {
     'selected_pt':selected_pt
   }
   $.ajax({
     type: 'GET',
     url: '/v1.0/project/select',
     data: data,
     async:false, // 동기방식으로 설정 -> 전역변수사용가능
     success: function(data){
       tp_list = data.result2;

       for(var i=0; i<tp_list.length; i++){
         var op = new Option();
         op.value = tp_list[i]['TP_number'];
         op.text = tp_list[i]['TP_name'];
         tp_combo.add(op);
       }

       $("#category").text(" // PlatForm : "+data.platform);
       category = data.platform;
       },
       error: function(request, status, error){
         alert(request.status+" "+request.statusText);
         //alert(request.responseText);
       }
   })
 })

 $('#tp_btn').click(function(){
     var selected_tp = $('#testprocedure').val();
     $('#testcase').children().nextAll().remove();
     $('#olist').children().remove();
     var data = {
         'selected_tp':selected_tp
     }
     $.ajax({
         type: 'GET',
         url: '/v1.0/testprocedure/select',
         data: data,
         success: function(data){
             tc_list = data.result2;
             for(var i=0; i<tc_list.length; i++){
               var op = new Option();
               op.value = tc_list[i]['TC_number'];
               op.text = tc_list[i]['TC_name'];
               tc_combo.add(op);
             }
         },
         error: function(request, status, error){
           alert(request.status+" "+request.statusText);
           //alert(request.responseText);
         }
     })
 })

 $('#tc_btn').click(function(){
     var selected_tc = $('#testcase').val();
     $('#olist').children().remove();
     $('#divTestButton').children().remove();
     $('#divTestButton').empty();
     $('#webTestResult').empty();
     var data = {
         'selected_tc':selected_tc
     }
     $.ajax({
         type: 'GET',
         url: '/v1.0/testcase/select',
         async: false,
         data: data,
         success: function(data){

         ts_list = data.result2;


         var passfail = "성공/실패"

         if(category == "DeskTop"){ // Desktop 프로그램



             for(var i=0; i<ts_list.length; i++){
             var passfail_Result = new Array(ts_list.length);

              if(ts_list[i]['ER_value'].indexOf('.PNG') >= 0){ // 예상결과가 이미지일 경우

function img_success_btn_click(){
               var selected_ts = $(this).parent().attr('step');
               $("td[step=\""+selected_ts+"\"] button").prop('disabled',true);
                var selected_tc = $('#testcase').val();

                passfail_Result[selected_ts-1] = 'Pass';

                if(selected_ts == ts_list.length){
                var data = {
                'selected_tc' :  selected_tc,
                'PF'          :  passfail_Result
                }
                $.ajax({
                   type: 'PUT',
                   url: '/v1.0/desktop/testExecution/PF',
                   data: JSON.stringify(data),
                   dataType : 'JSON',
                   contentType: "application/json",
                   success: function(data){
                     alert(passfail_Result);
                   },
                   error: function(request, status, error){
                     alert(request.status+" "+request.statusText);
                     //alert(request.responseText);
                   }
                })

                if(number_of_fail >0){
                $("#testResult").children().remove()
                $("#testResult").text("테스트 결과 : 실패")


                }
                else{

                $("#testResult").children().remove()
                $("#testResult").text("테스트 결과 : 성공")

                }

                }





               }

function img_fail_btn_click(){
                var selected_tc = $('#testcase').val();
                var selected_ts = $(this).parent().attr('step');
                $("td[step=\""+selected_ts+"\"] button").prop('disabled',true);

                passfail_Result[selected_ts-1] = 'Fail';
                number_of_fail = number_of_fail +1;

                if(selected_ts == ts_list.length){
                var data = {
                'selected_tc' :  selected_tc,
                'PF'          :  passfail_Result
                }
                $.ajax({
                   type: 'PUT',
                   url: '/v1.0/firstExecution/update',
                   data: JSON.stringify(data),
                   dataType : 'JSON',
                   contentType: "application/json",
                   success: function(data){
                     alert(passfail_Result);
                   },
                   error: function(request, status, error){
                     alert(request.status+" "+request.statusText);
                     //alert(request.responseText);
                   }
                })

                if(number_of_fail >0){
                $("#testResult").children().remove()
                $("#testResult").text("테스트 결과 : 실패")


                }
                else{

                $("#testResult").children().remove()
                $("#testResult").text("테스트 결과 : 성공")

                }


                }



               }


              const tr1 = $("<tr>").append($("<th>").text(i+1)).append($("<td>").text(ts_list[i]['TS_info']).attr('colspan','2'))
              const tr2 = $("<tr>").append($("<th id = 'passfail' >").text(passfail).attr('step',i+1).attr('rowspan','3')).append($("<td>").text("예상 결과")).append($("<td>").text("실제 결과"))
              const tr3 = $("<tr>").append($("<td>").append($("<img src =\""+ts_list[i]['ER_value']+"\" width ='50%'>")))
              .append($("<td class = 'insert_actual_img' >"))

              const desktop_img_success_btn = $("<button class = 'img_success' id ='img_success'>성공</button>").attr('step',i+1)
              .click(img_success_btn_click)

              const desktop_img_fail_btn    = $("<button class = 'img_fail'>실패</button>")
              .click(img_fail_btn_click)

              reRecording_btn = $("<button id ='reRecording_btn' class = 'btn btn-primary btn-md ml-5'>reRecording</button>")
              .click(desktop_reRecording_btn_click)
              const tr4 = $("<tr>").append($("<td>").text("두 사진이 같으면 '성공' , 다르면 '실패' ").attr('colspan','2').attr('step',i+1)
              .append(desktop_img_success_btn).append(desktop_img_fail_btn).append(reRecording_btn)
               );

               $('#olist').append(tr1).append(tr2).append(tr3).append(tr4);
               }
               else{ // 예상결과가 이미지가 아닐 경우
               const notr1 = $("<tr>").append($("<th>").text(i+1).attr('rowspan','3')).append($("<td>").text(ts_list[i]['TS_info']).attr('colspan','2'))
               const notr2 = $("<tr>").append($("<td>").text("예상 결과 : "+ts_list[i]['ER_value']).attr('colspan','2'))
               const firstExecution_btn = $("<button class = 'firstExecution' id ='firstExecution_btn'>초기수행(recording)</button>").click(desktop_firstExecution_btn_click)
               const desktop_teststep_test_btn = $("<button class = 'desktop_teststep_test_btn'>테스트</button>").click(desktop_test_btn_click)

               const notr3 = $("<tr>").append($("<td>")
               .text("테스트 결과 : ").append($("<div id = test_result>").attr('step',i+1)).attr('colspan','2'))

              $('#olist').append(notr1).append(notr2).append(firstExecution_btn).append(testbtn).append(notr3);
               }
            }
            // desktop 프로그램 테스트 스텝 전부 수행하는 버튼
            const result = $("#testResult").text("테스트 결과 : ");

            const desktop_test_btn = $("<button class = 'btn btn-primary btn-md ml-1' id ='firstExecutionbtn'>TEST</button>")
            .click(desktop_test_btn_click)

            const desktop_recording_btn = $("<button class = 'btn btn-primary btn-md ml-5'>recording</button>")
            .click(desktop_recording_btn_click)

            $('#divTestButton').append(desktop_recording_btn).append(desktop_test_btn);

            } // Desktop 프로그램 끝

            else{ // Web 프로그램 시작
             ts_list = data.result2;
             var i=0;
             for(; i<ts_list.length; i++){

             const tr1 = $("<tr>").append($("<th>").text(i+1).attr('rowspan','2')).append($("<td>").text(ts_list[i]['TS_info']));
             const tr2 = $("<tr>").append($("<td>").text("예상 결과 : "+ts_list[i]['ER_info']));
             $('#olist').append(tr1).append(tr2);
            }
               if(ts_list[i-1]['ER_value'].indexOf('.PNG') >= 0){ // 예상결과가 이미지일 경우
             const expectedResult_tr1 = $("<tr>").append($("<th>").text("예상 결과 이미지")).append($("<th>").text("실제 결과 이미지"));
             const expectedResult_tr2 = $("<tr>").append($("<td>").append($("<img src =\""+ts_list[i-1]['ER_value']+"\" width ='100%'>")))
             .append($("<td id = web_actual_result_img>"));

             $('#webTestResult').append(expectedResult_tr1).append(expectedResult_tr2);
             }

             const web_test_btn = $("<button class = 'btn btn-primary btn-md ml-1' id ='web_test_btn'>TEST</button>").click(web_test_btn_click)
             const result = $("#testResult").text("테스트 결과 : ㅁㄴㅇ");

             const web_recording_btn = $("<button class = 'btn btn-primary btn-md ml-5'>recording</button>")
             .click(web_recording_btn_click)

                $('#divTestButton').empty();
                if(category != 'DeskTop'){
                    $('#divTestButton').append(web_recording_btn).append(web_test_btn).append(result);
                }
            } // WEB 프로그램 끝
            }
           ,
         error: function(request, status, error){
           alert(request.status+" "+request.statusText);
           //alert(request.responseText);
         }

      })
   })

function desktop_firstExecution_btn_click(){
               var num = this.parentNode.parentNode.previousSibling.firstChild.innerText;
               var selected_tc = $('#testcase').val();
               var selected_ts = num;
               var data = {
                   'selected_tc':selected_tc,
                   'selected_ts':selected_ts
               }
               $.ajax({
                   type: 'PUT',
                   url: '/v1.0/firstExecution/update',
                   data: JSON.stringify(data),
                   dataType : 'JSON',
                   contentType: "application/json",
                   success: function(data){
                   console.log("success");
                   },
                   error: function(request, status, error){
                     alert(request.status+" "+request.statusText);
                     //alert(request.responseText);
                   }
                })
               }

function desktop_teststep_test_btn_click(){
               var selected_tc = $('#testcase').val();
               var selected_ts = $(this).parent().attr('step');
//               var selected_ts = num;
               var data = {
                   'selected_tc':selected_tc,
                   'selected_ts':selected_ts
               }
               $.ajax({
                   type: 'PUT',
                   url: '/<version>/testExecution/update',
                   data: JSON.stringify(data),
                   dataType : 'JSON',
                   contentType: "application/json",
                   async : false,
                   success: function(data){
                   $("#test_result").append($("th[step=\""+selected_ts+"\"").text(data.PF));
//                     $("#test_result").text(data.PF);
                   },
                   error: function(request, status, error){
                     alert(request.status+" "+request.statusText);
                     //alert(request.responseText);
                   }
                })
               }

function desktop_test_btn_click(){
               var selected_tc = $('#testcase').val();
               var data = {
                   'selected_tc':selected_tc
               }
               $.ajax({
                   type: 'PUT',
                   url: '/v1.0/desktop/testExecution/update',
                   data: JSON.stringify(data),
                   dataType : 'JSON',
                   contentType: "application/json",
                   async:false,
                   success: function(data){
                   var test = new Array(ts_list.length);
                   $('.desktop_actual_result_img').remove();
                   $('.insert_actual_img').each(function(index,item){
                   $(item).append($("<img src = \""+data.actualImg[index]+"\" width ='50%'>"))
                   console.log(data.actualImg[index]);
                   })
                   },
                   error: function(request, status, error){
                     alert(request.status+" "+request.statusText);

                     //alert(request.responseText);
                   }
                 })
               }

function desktop_recording_btn_click(){
               var selected_tc = $('#testcase').val();
               var data = {
                   'selected_tc':selected_tc
               }
               $.ajax({
                   type: 'PUT',
                   url: '/v1.0/desktop/firstExecution/update',
                   data: JSON.stringify(data),
                   dataType : 'JSON',
                   contentType: "application/json",
                   success: function(data){
                   //alert(selected_tc);

                   },
                   error: function(request, status, error){
                     alert(request.status+" "+request.statusText);

                     //alert(request.responseText);
                   }
                 })
               }

function desktop_reRecording_btn_click(){
              var selected_tc = $('#testcase').val();
              var selected_ts = $(this).parent().attr('step');
              var data = {
                    'selected_tc' : selected_tc,
                    'selected_ts' : selected_ts
              }
              $.ajax({
                   type: 'PUT',
                   url: '/v1.0/desktop/afterExecution/reRecording',
                   data: JSON.stringify(data),
                   dataType : 'JSON',
                   contentType: "application/json",
                   success: function(data){



                   }
                   ,
                   error: function(request, status, error){
                     alert(request.status+" "+request.statusText);
                     //alert(request.responseText);
                   }
                    })

}

function web_test_btn_click(){
             const web_test_success_btn = $("<button class = 'btn btn-primary btn-md ml-1' id = 'web_test_success_btn >성공</button>")
             const web_test_fail_btn = $("<button class = 'btn btn-primary btn-md ml-1' id = 'web_test_fail_btn >실패</button>")

               var selected_tc = $('#testcase').val();
               var selected_pt = $('#project').val();
               var data = {
                   'selected_tc':selected_tc,
                   'selected_pt':selected_pt
               }
               $.ajax({
                   type: 'PUT',
                   //url: '/v1.0/testExecution/testweb',
                   url: '/v1.0/web/testExecution/update',
                   data: JSON.stringify(data),
                   dataType : 'JSON',
                   contentType: "application/json",
                   success: function(data){
                    $('#web_actual_result_img').append($("<img src =\""+data.actualImg+"\" width ='100%'>"))
                    $('#webTestResult').append($("<td>")
                    .append(web_test_success_btn // test 버튼-> 성공 버튼 클릭시
                    .click(web_test_success_btn_click))
                    .append(web_test_fail_btn// test 버튼 -> 실패 버튼 클릭 시
                    .click(web_test_fail_btn))
                    )

                   }
                   ,
                   error: function(request, status, error){
                     alert(request.status+" "+request.statusText);

                     //alert(request.responseText);
                   }
                    })
               }
function web_test_success_btn_click(){
               var selected_tc = $('#testcase').val();
               var selected_pt = $('#project').val();
               var data = {
                   'selected_tc':selected_tc,
                   'selected_pt':selected_pt
               }
               $.ajax({
                   type: 'PUT',
                   url: '/v1.0/test/asd',
                   data: JSON.stringify(data),
                   dataType : 'JSON',
                   contentType: "application/json",
                   success: function(data){
                   alert("success");

                   },
                   error: function(request, status, error){
                     alert(request.status+" "+request.statusText);

                     //alert(request.responseText);
                   }
                    })
                    }
function web_test_fail_btn_click(){
               var selected_tc = $('#testcase').val();
               var selected_pt = $('#project').val();
               var data = {
                   'selected_tc':selected_tc,
                   'selected_pt':selected_pt
               }
               $.ajax({
                   type: 'PUT',
                   url: '/v1.0/test/dsa',
                   data: JSON.stringify(data),
                   dataType : 'JSON',
                   contentType: "application/json",
                   success: function(data){
                   alert("fail");

                   },
                   error: function(request, status, error){
                     alert(request.status+" "+request.statusText);

                     //alert(request.responseText);
                   }
                 })
               }
function web_recording_btn_click(){
               var selected_tc = $('#testcase').val();

               var data = {
                   'selected_tc':selected_tc

               }
               $.ajax({
                   type: 'PUT',
                   url: '/v1.0/web/firstExecution/update',
                   data: JSON.stringify(data),
                   dataType : 'JSON',
                   contentType: "application/json",
                   success: function(data){
                   //alert(selected_tc);

                   },
                   error: function(request, status, error){
                     alert(request.status+" "+request.statusText);

                     //alert(request.responseText);
                   }
                 })
               }