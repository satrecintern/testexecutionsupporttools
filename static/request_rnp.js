$('#btnPlayback').click(()=>{
    js_data = {testcasename : $('#textTestCaseName').val(), stepcount:$('#stepCount').val()};
    send_data = JSON.stringify(js_data);
   $.post('/playback',send_data)
    .done(response=>
            $("#log").append($("<dd>").attr('class','col-7').text(response.msg))
                     .append($("<dd>").attr('class','col-5').text(response.data))
     );
});
$('#btnRecord').click(()=>{
    js_data = {testcasename : $('#textTestCaseName').val(), stepcount:$('#stepCount').val()};
    send_data = JSON.stringify(js_data);
    $.post('/record',send_data)
     .done(response=>
            $("#log").append($("<dd>").attr('class','col-7').text(response.msg))
                     .append($("<dd>").attr('class','col-5').text(response.data))
     );
});

$('.testcase').click(function(e){
    $('#textTestCaseName').val($(this).text());
})

