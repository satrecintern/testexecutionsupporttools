from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import os.path as path
import os
import time
import win32com.client
import win32con
import pyperclip
import pyautogui

from PIL import ImageGrab
import win32gui
import yaml

global resultType
global testcase
global text_args



with open("../../recording_playback_Route.yml") as f:
    dir_route = yaml.load(f,Loader=yaml.SafeLoader)

TEST_RESULT_DIR = dir_route['TEST_RESULT_DIR']
test_result_path="../../"+TEST_RESULT_DIR

def capture():
    fname = testcase + '.png'
    hwnd = win32gui.GetForegroundWindow()
    win32gui.MoveWindow(hwnd, 365, 110, 1040, 900, True)
    time.sleep(0.5)
    region=(0, 0, 1800, 1056)
    locate = pyautogui.locateOnScreen("scroll.PNG",region=region)
    if locate is None:
        hwnd = win32gui.GetForegroundWindow()
        dimensions = win32gui.GetWindowRect(hwnd)
        image = ImageGrab.grab(dimensions, include_layered_windows=False, all_screens=True)
        image.save(test_result_path + fname)
    else:
        url = get_url()
        chrome_options = Options()
        chrome_options.add_argument('headless')
        chrome_options.add_argument('--start-maximized')
        driver = webdriver.Chrome(options=chrome_options)
        driver.get(url)
        required_width = driver.execute_script('return document.body.parentNode.scrollWidth')
        required_height = driver.execute_script('return document.body.parentNode.scrollHeight')
        driver.set_window_size(required_width, required_height)
        driver.find_element_by_tag_name('body').screenshot(test_result_path +fname)
        driver.quit()

def get_url():
    pyautogui.hotkey('alt', 'd')
    pyautogui.hotkey('ctrl', 'c')
    url = pyperclip.paste()
    return url

def pytest_addoption(parser):
    parser.addoption("--testcase", action="store", default="ADDOwner기능",
                     help="my option: ")

def pytest_cmdline_main(config):
    global testcase
    testcase = config.getoption("--testcase")

def pytest_runtest_call(item):
    shell = win32com.client.Dispatch("WScript.Shell")
    shell.AppActivate("PetClinic :: a Spring Framework demonstration - Chrome")

    hwnd = win32gui.GetForegroundWindow()
    win32gui.MoveWindow(hwnd, -1450,100,1040,1010,True)


def pytest_runtest_teardown(item):
    global testcase

    fname =test_result_path+testcase+'.png'
    if path.isfile(fname):
        os.remove(fname)
    # 캡쳐
    capture()



