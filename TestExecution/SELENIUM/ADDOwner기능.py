# Generated by Selenium IDE
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

class TestADDOwner():
  def setup_method(self, method):
    self.driver = webdriver.Chrome()
    self.vars = {}
  
  def teardown_method(self, method):
    self.driver.quit()
  
  def test_aDDOwner(self):
    self.driver.get("http://localhost:8089/")
    self.driver.set_window_size(974, 1040)
    self.driver.find_element(By.CSS_SELECTOR, "li:nth-child(2) > a").click()
    self.driver.find_element(By.LINK_TEXT, "Add Owner").click()
    self.driver.find_element(By.ID, "firstName").click()
    self.driver.find_element(By.ID, "firstName").send_keys("IN")
    self.driver.find_element(By.ID, "lastName").click()
    self.driver.find_element(By.ID, "lastName").send_keys("YERIN")
    self.driver.find_element(By.ID, "address").click()
    self.driver.find_element(By.ID, "address").send_keys("23458")
    self.driver.find_element(By.ID, "city").click()
    self.driver.find_element(By.ID, "city").send_keys("SEOUL")
    self.driver.find_element(By.ID, "telephone").click()
    self.driver.find_element(By.ID, "telephone").send_keys("01023924100")
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
  
