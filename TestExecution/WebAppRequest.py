from os import listdir, getcwd
import os
import yaml
import os.path as path
import re

with open('./recording_playback_Route.yml') as f:
    dir_route = yaml.load(f,Loader=yaml.SafeLoader)

SELENIUM_EXE = getcwd()+'\\'+dir_route['SELENIUM_EXE']
AHK_EXE = getcwd()+'\\'+dir_route['AHK_EXE']

WIN_START_RECORD_EXE = getcwd()+'\\'+dir_route['WIN_START_RECORD_EXE']
WIN_STOP_RECORD_EXE = getcwd()+'\\'+dir_route['WIN_STOP_RECORD_EXE']

WEB_TEST_DIR = getcwd()+'\\'+ dir_route['WEB_TEST_DIR']

def recordTest(testcase,url):
    execute_arguments = WIN_START_RECORD_EXE+" "+testcase +" " +url
    print('run : '+ execute_arguments)
    os.system(execute_arguments)

def saveTest(testcase):
    print('save process : wait for 2 F9')
    fname = WEB_TEST_DIR+testcase + '.py'
    if path.isfile(fname):
        print('recording된 파일이 존재합니다.')
        os.remove(fname)
    execute_arguments = WIN_STOP_RECORD_EXE+" "+testcase+ " " +WEB_TEST_DIR
    print('run : '+ execute_arguments)
    os.system(execute_arguments)

def runTest(testcase):
    os.chdir(WEB_TEST_DIR)
    execute_arguments = "pytest "+testcase+ ".py"+" "+"-q --testcase="+testcase
    os.system(execute_arguments)
    os.chdir('../../')

def recordAndSaveTestCase(testcase, url):
    recordTest(testcase,url)
    saveTest(testcase)


def click_improve(testcase):
    execute_arguments=WEB_TEST_DIR+testcase+ ".py"
    f = open(execute_arguments, 'r')
    text = f.readlines()
    f.close()

    f2 = open(execute_arguments, 'w')

    pattern = "\).click\(\)"
    pattern2 = "    "
    for line in text:
        matchOB = re.search(pattern, line)
        if matchOB:
            f2.write(pattern2+'element=' + line[4:matchOB.start()+1] + '\n')
            f2.write(pattern2+'self.driver.execute_script("arguments[0].click();", element)\n')
        else:
            f2.write(line)
    f2.close()
