# Windows Application Recorder #

## Description ##
Windows Application Record 및 Playback을 위한 Python Program

## Dependency ##
* Python 3.8

## Python Package ##
* keyboard

## Requirements ##
* Pulover’s Macro Creator 가 실행 중
* Target Program이 실행 중

---

# Constants #
### PMC_BASE ###
* 프로젝트 폴더 내 Pulover’s Macro Creator가 존재하는 폴더의 상대 경로

### AHK_BASE ### 
* 프로젝트 폴더 내 AutoHotKey가 존재하는 폴더의 상대 경로

### PMC_EXE ###
* 프로젝트 폴더 내 Pulover’s Macro Creator 프로그램의 상대 경로

### AHK_EXE ###
* 프로젝트 폴더 내 AutoHotKey 프로그램의 상대 경로

### WIN_START_RECORD_EXE ###
* 프로젝트 폴더 내 Record를 시작시켜주는 AutoHotKey 코드의 경로

### WIN_STOP_RECORD_EXE ###
* 프로젝트 폴더 내 Record를 정지시켜주는 AutoHotKey 코드의 경로

### WIN_TEST_DIR ### 
* 기록된 PMC 파일의 저장 경로

 ---

# Functions #

### getTestList() → map ###
* PMC 경로를 읽어서 해당 경로에 있는 TestStep PMC 파일을 모두 로드 
이로부터 TestCase별 TestStep 수를 읽고, TestCase별 Step수를 모두 기록한 map을 반환
* return value : ```{ ‘test1’ : 4, ‘test2’ : 6, ‘test3’ : 2 … }```
 

### * findValidateObject(string:xpath) → string  <UNIMPLEMENTED> ###
* xpath를 받아서 해당 xpath에 저장된 String 정보를 찾아서 반환

 

### runTestStepsWithFindResult(string:testcase, int:step_count, list:actual_value_locations) → list ###
* TestCase를 입력 받아서 해당 TestCase에 대한 모든 TestStep을 실행
* 각 Step의 실행 후 actual_value_locations로부터 Actual Result를 담고 있는 경로에 대한 정보를 읽어서  Actual Result를 찾아서 저장
* 모든 Step이 실행 되면 Step 별 Actual Result가 저장된 List를 반환
* return value : ```["1", "Hello World", "Test OK"]```

### runTest(string:teststep) ###
* TestStep을 입력받아서 TestStep을 실행

### recordTest() ###
* Record를 시작한다
* 메세지가 뜨고 나서 F9를 누르면 기록 시작
* 한번 더 F9를 누르면 기록 정지

### saveTest(teststep) → string ###
* teststep이라는 이름으로 PMC 파일로 저장
* 저장 경로 : 프로그램 폴더/PMC/testdata/
* 저장된 teststep의 절대경로를 반환
* return value : ```C:\\TestTools\\TestExecution\\PMC\\testdata\\test1_1.pmc```
 

### recordAndSaveTestSteps(testcase, step_count) → list ###
* testcase에 대한 teststep을 순차적으로 기록
* step_count만큼 연속해서 기록
* 각 파일은 testcase_n.pmc 형태로 저장됨
* return value
```
[
  "C:\\TestTools\\TestExecution\\PMC\\testdata\\test1_1.pmc",
  "C:\\TestTools\\TestExecution\\PMC\\testdata\\test1_2.pmc",
  "C:\\TestTools\\TestExecution\\PMC\\testdata\\test1_3.pmc",
  "C:\\TestTools\\TestExecution\\PMC\\testdata\\test1_4.pmc"
]
```
 