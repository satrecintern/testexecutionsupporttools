﻿#include %A_ScriptDir%\Windows_Overlay.ahk

PULOVER = Pulover's Macro Creator v5.3.9

If (%0% == 0){
    MsgBox, Error : TestCase 정보가 없습니다. 동작을 종료합니다.
    return
}
If (%0% == 1){
    MsgBox, Error : 저장 경로 정보가 없습니다. 동작을 종료합니다.
    return
}

testCaseName := A_Args[1]
dir := A_Args[2]

#NoEnv
SetWorkingDir %A_ScriptDir%
CoordMode, Mouse, Window
SendMode Input
#SingleInstance Force
SetTitleMatchMode 2
#WinActivateForce
SetControlDelay 1
SetWinDelay 0
SetKeyDelay -1
SetMouseDelay -1
SetBatchLines -1

;overlay setting
overlay_init()

overlay_show()

WinShow, %PULOVER%
Winset, Alwaysontop, , %PULOVER%
WinActivate, %PULOVER%
Sleep, 1000
Click, 30,40 Left
Sleep 80
Click, 30,110 Left
WinWait, Save Project
Sleep 100
Click, 125, 45 Left
Send, %dir%
Sleep 100
Send, {Enter}
Sleep, 200
Click 140, 440 Left
Send, %testCaseName%
Send, {Enter}
WinWait, Confirm file replace, , 0.5
IfWinExist, Confirm file replace
{
	WinActivate, Confirm file replace
	Sleep 1000
	Send, {Left}
	Sleep 100
	Send, {Enter}
}
WinWait, %PULOVER%
WinActivate, %PULOVER%
Sleep, 500
Click, 30,40 Left
Sleep, 100
Click, 30,70 Left
Sleep, 100
WinHide, %PULOVER%
Sleep, 100
ExitApp