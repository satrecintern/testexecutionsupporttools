﻿#include %A_ScriptDir%\Windows_Overlay.ahk

PULOVER =Pulover's Macro Creator v5.3.9
global timeout = 0
global recorded = 0

If (%0% == 0){
    timeout = 40
}
else{
    timeout := A_Args[1]
}

#NoEnv
SetWorkingDir %A_ScriptDir%
CoordMode, Mouse, Window
SendMode Input
#SingleInstance Force
SetTitleMatchMode 2
#WinActivateForce
SetControlDelay 1
SetWinDelay 0
SetKeyDelay -1
SetMouseDelay -1
SetBatchLines -1
DetectHiddenWindows, On

;overlay setting
overlay_init()
overlay_show()

IfWinNotExist, %PULOVER%
{
    Run ../PMC/MacroCreator.exe
}

WinShow, %PULOVER%
WinWait, %PULOVER%
WinActivate, %PULOVER%
Click, 50,40 Left
Sleep 100
Click, 50,70 Left
Sleep 500
WinWait, MacroCreator.exe, , 3
IfWinExist, MacroCreator.exe
{
    WinActivate, MacroCreator.exe
    Send, {Enter}
    WinHide, %PULOVER%
}
Sleep, 1000
overlay_hide()
MsgBox, 테스트할 프로그램을 켜고, `n선택하고 F9를 눌러 기록하세요, `n끝낼 경우 F9를 눌러서 끝내세요`n기록을 중단하시려면 ESC를 누르세요
KeyInputCheck()
recorded=1
KeyInputCheck()
ExitApp, 0

PressF9IfRecorded()
{
	if (recorded == 1)
	{
		Send, {F9}
	}
}

KeyInputCheck()
{
	state = 0
	KeyWait, F9, DT%timeout%
	state := ErrorLevel
	KeyWait, F9, L
	if (state == 1) ; timeout
	{
		PressF9IfRecorded()	
		ExitApp, 1
	}
}


ESC::
PressF9IfRecorded()
ExitApp, 2