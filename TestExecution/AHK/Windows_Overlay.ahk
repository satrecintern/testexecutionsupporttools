﻿overlay_init()
{
global

borderless_screenWidth = % A_ScreenWidth-14
borderless_screenHeight = % A_ScreenHeight-14

text_pos_x = % Round(borderless_screenWidth * 0.7)
text_pos_y = % Round(borderless_screenHeight * 0.7)
text_size = % Round(0.02 * A_ScreenWidth)

; Draw Text
Gui, Color, %CustomColor%
Gui, Font, s%text_size%
Gui, Add, Text, x%text_pos_x% y%text_pos_y% vMyText cRed, Macro Playing...

; Set Window
Gui, +toolwindow +resize -caption +alwaysontop
Gui, color , 000000  ; set color value RGB
Gui, Show, x0 y0 w%borderless_screenWidth% h%borderless_screenHeight%

; Transparent and Non-clickable
; https://www.reddit.com/r/AutoHotkey/comments/ajx7og/help_make_a_window_on_top_opaque_and_not_clickable/
Gui, +lastFound
WinSet, Style, -0xC40000 , A
WinSet, Transparent, 0
WinSet Exstyle, ^0x20

}

overlay_show(){
Gui, +lastFound
WinSet, Transparent, 130
Sleep, 1000
}

overlay_hide(){
Gui, +lastFound
WinSet, Transparent, 0
}

IfEqual A_ScriptName, Windows_Overlay.ahk
{
overlay_init()
overlay_show()
}