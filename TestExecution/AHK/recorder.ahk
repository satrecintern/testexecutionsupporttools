﻿#include EveryHangul.ahk

testCase := A_Args[1]
URL := A_Args[2]

#NoEnv
SetWorkingDir %A_ScriptDir%
CoordMode, Mouse, Window
SendMode Input
#SingleInstance Force
SetTitleMatchMode 2
#WinActivateForce
SetControlDelay 1
SetWinDelay 0
SetKeyDelay -1
SetMouseDelay -1
SetBatchLines -1

GUI()
{
Gui, Add, Button, x80 y55 w45 h25 gBtn, ok
Gui, Add, Text, x51 y20 w200 h30, recording start
Gui, Show, x2764 y437 w200 h90, recorder
return
}

GUI()
return


Btn:
Gui,destroy
Run, chrome.exe https://www.naver.com/
WinActivate, Pulover's Macro Creator ahk_class AutoHotkeyGUI
Sleep, 600
WinActivate, NAVER - Chrome ahk_class Chrome_WidgetWin_1
Sleep, 600
MouseClick, Left, 1811,59
Sleep, 600
WinActivate, NAVER - Chrome ahk_class Chrome_WidgetWin_1
Sleep, 600
Send ^w
Sleep, 600
WinWait, Selenium IDE ahk_class Chrome_WidgetWin_1
WinActivate, Selenium IDE ahk_class Chrome_WidgetWin_1
WinMove, Selenium IDE ahk_class Chrome_WidgetWin_1,,3050,77,766,963
Sleep, 800
MouseClick, Left, 245,267
Sleep, 600
한영변환("영")
Sleep, 600
SendRaw, % testCase
Sleep, 600
SendInput {enter}
Sleep, 600
WinActivate, Selenium IDE - %testCase% ahk_class Chrome_WidgetWin_1
Sleep, 600
SendRaw, % URL
SendInput {enter}
Sleep, 600
WinWait, PetClinic :: a Spring Framework demonstration - Chrome
WinMove, PetClinic :: a Spring Framework demonstration - Chrome,,1944,104
ExitApp
