﻿#include %A_ScriptDir%\Gdip_All.ahk

Sleep, 300
Send, {Alt Down}{PrintScreen}{Alt Up}
Sleep, 333
pToken := Gdip_Startup()
pBitmap := Gdip_CreateBitmapFromClipboard()
sBitmap := Gdip_SaveBitmapToFile(pBitmap,  A_ScriptDir "\..\..\static\actualimg\" A_Args[1] ".png", 100)
dBitmap := Gdip_DisposeImage(pBitmap)
sToken := Gdip_Shutdown(pToken)