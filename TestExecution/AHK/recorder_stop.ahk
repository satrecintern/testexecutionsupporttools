﻿#include EveryHangul.ahk

testCase := A_Args[1]
route := A_Args[2]

#NoEnv
SetWorkingDir %A_ScriptDir%
CoordMode, Mouse, Window
SendMode Input
#SingleInstance Force
SetTitleMatchMode 2
#WinActivateForce
SetControlDelay 1
SetWinDelay 0
SetKeyDelay -1
SetMouseDelay -1
SetBatchLines -1


GUI()
{
Gui, Add, Button, x80 y55 w45 h25 gBtn, ok
Gui, Add, Text, x51 y20 w200 h30, recording stop
Gui, Show, x2764 y437 w200 h90, recorder
return
}

F9::
GUI()
return

Btn:
Gui,destroy
WinActivate, Selenium IDE - %testCase%* ahk_class Chrome_WidgetWin_1
Sleep, 600
MouseClick, Left, 740,83
Sleep, 600
SendRaw, % testCase
Sleep, 600
SendInput {enter}
Sleep, 600
WinActivate, Selenium IDE - %testCase%* ahk_class Chrome_WidgetWin_1
Sleep, 600
MouseClick, Left, 176,156
Sleep, 600
MouseClick, Left, 98,310
Sleep, 600
MouseClick, Left, 215,374
Sleep, 600
MouseClick, Left, 445,574
Sleep, 600
WinActivate, 다른 이름으로 저장 ahk_class #32770
WinMove, 다른 이름으로 저장 ahk_class #32770,,2887,77,960,540
SendRaw, % testCase
Sleep, 600
MouseClick, Left, 275,51
Sleep, 600
SendRaw, % route
Sleep, 1200
SendInput {enter}
Sleep, 600
MouseClick, Left, 783,510
Sleep, 600
WinActivate, Selenium IDE - %testCase%* ahk_class Chrome_WidgetWin_1
Sleep, 600
MouseClick, Left, 735,19
Sleep, 600
WinActivate, 앱을 종료하시겠습니까? ahk_class Chrome_WidgetWin_1
Sleep, 600
MouseClick, Left, 322,106
Sleep, 600
ExitApp

~*Lbutton::
CoordMode, Mouse, Screen
Sleep, 300
MouseGetPos, vx,vy
Sleep, 300
CoordMode, Mouse, Window
return

Delete_TextClick(x,y,c,d)
{
;MsgBox, %x%, %y%
a:=% c
b:=% d
count1:=0
count2:=0
jump_range1:=(a-x)/13
jump_range2:=(b-y)/13
;MsgBox, jmp%jump_range1%
;MsgBox, count%count%
Loop
{

	if(a<x){
		a:=x
		count1++
	}

	if(count1<1){
		a:=a-30
	}


if(count1>0){
	if(b<y){
		b:=y
		count2++
		}

	if(count2<1){
		b:=b-30
	}

	if(count2>0){
	MouseClick, Left, %a%,%b%
	Break
	}
}
;MsgBox, a and b == %a%, %b%
;MsgBox, count and count == %count%
Click %a%,%b%, 0
Sleep, 1
}
}

Delete::
x:=% vx
y:=% vy
;MsgBox, %x%, %y%
WinActivate, Selenium IDE ahk_class Chrome_WidgetWin_1
WinMove, Selenium IDE ahk_class Chrome_WidgetWin_1,,3050,77,766,963
Sleep, 600
MouseClick, Left, 317,779
Sleep, 600
SendRaw, send keys
Sleep, 300
SendInput {enter}
Sleep, 300
MouseClick, Left, 307,854
Sleep, 600
SendRaw, ${KEY_CONTROL}a${KEY_DELETE}
Sleep, 600
MouseClick, Left, 626,818
Sleep, 1500
SetDefaultMouseSpeed,0
CoordMode, Mouse, Screen
Delete_TextClick(x, y,3673, 892)
SetDefaultMouseSpeed,0
WinWaitActive, Selenium IDE ahk_class Chrome_WidgetWin_1
WinWaitActive, PetClinic :: a Spring Framework demonstration - Chrome
MouseClick, Left,% x, % y
CoordMode, Mouse, Window
Sleep, 500
Send ^a
Sleep, 300
Send {Delete}
Sleep, 600
return