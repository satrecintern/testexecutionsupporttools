from os import listdir, getcwd
from os.path import isfile, join
import subprocess
import yaml
import enum
import os

'''
Enum
'''


class KeyEventResult(enum.Enum):
    INIT = -1
    SUCCESS = 0
    TIMEOUT = 1
    BREAK = 2


'''
Constants
'''

with open('./TestExecution\\WinAppRequest.yml') as yml:
    WIN_CONST = yaml.safe_load(yml)

# get absoulte directory from os
WIN_CONST['TEST_DIR'] = getcwd()+'\\' + WIN_CONST['TEST_DIR']
# WIN_CONST['RESULT_DIR'] = getcwd()+'\\' + WIN_CONST['RESULT_DIR']


'''
Functions
'''


def getTestList() -> dict:
    file_dict = {}
    for f in listdir(WIN_CONST['TEST_DIR']):
        if isfile(join(WIN_CONST['TEST_DIR'], f)):
            testcase_name = f.split("_")[0]
            if testcase_name in file_dict.keys():
                file_dict[testcase_name] = file_dict[testcase_name] + 1
            else:
                file_dict[testcase_name] = 1
    return file_dict


def findValidateObject(xpath: str):
    #raise NotImplementedError
    return "unimplemented"


def runProgram(program: str) -> int:
    child_process = subprocess.Popen(program, stdout=subprocess.PIPE, shell=True)
    return child_process.pid


def killProgram(pid: int):
    subprocess.Popen("TASKKILL /F /PID {pid} /T".format(pid=pid))


def playbackTestSteps(testcase: str) -> list:
    file_dict = getTestList()
    count = file_dict[testcase]
    result = []
    for i in range(1, int(count)+1):
        playbackTest(testcase + "_" + str(i))
        result.append(i)
    return result


def playbackTestStepsWithFindResult(testcase: str, step_count: int) -> list:
    result = []
    for i in range(0, int(step_count)):
        playbackTest(testcase + "_" + str(i + 1))
        result.append(captureScreen(testcase + "_" + str(i + 1)))
    return result


def playbackTest(test_name: str):
    test_filename = WIN_CONST['TEST_DIR'] + test_name + ".pmc"
    print('run : ' + WIN_CONST['PLAYER_EXE'] + ' ' + test_filename + ' -s')
    subprocess.check_output([WIN_CONST['PLAYER_EXE'], test_filename, "-s"])


def playbackTestWithCaseAndStep(test_case: str, test_step: str):
    playbackTest(test_case + "_" + test_step)


def recordTest():
    execute_arguments = [WIN_CONST['AHK_EXE'], WIN_CONST['START_RECORD_EXE'], WIN_CONST['TIMEOUT']]
    error_code = None
    print('run : '+' '.join(execute_arguments))
    try:
        subprocess.check_output(execute_arguments)
    except subprocess.CalledProcessError as error:
        error_code = KeyEventResult(error.returncode)

    if error_code is not None:
        if error_code is KeyEventResult.TIMEOUT:
            raise TimeoutError("Exceed Recording Time Limit " + WIN_CONST['TIMEOUT'])
        elif error_code is KeyEventResult.BREAK:
            raise InterruptedError("Break Recording")
    return


def saveTest(test_name: str) -> str:
    print('save process : wait for two F9 typing 30s')
    execute_arguments = [WIN_CONST['AHK_EXE'], WIN_CONST['STOP_RECORD_EXE'], test_name, WIN_CONST['TEST_DIR']]
    print('run : ' + ' '.join(execute_arguments))
    subprocess.check_output(execute_arguments)
    return WIN_CONST['TEST_DIR'] + test_name + ".pmc"


def saveTestWithCaseAndStep(test_case: str, test_step: str) -> str:
    return saveTest(test_case + "_" + test_step)


def recordAndSaveTestSteps(testcase: str, step_count: int) -> list:
    result = list()
    for i in range(1, int(step_count) + 1):
        recordTest()
        code_path = saveTest(testcase + "_" + str(i))
        result.append(code_path)
    return result


def captureScreen(test_name: str) -> str:
    execute_arguments = [WIN_CONST['AHK_EXE'], WIN_CONST['CAPTURE_EXE'], test_name]
    print('run : ' + ' '.join(execute_arguments))
    subprocess.check_output(execute_arguments)
    return WIN_CONST['RESULT_DIR'] + test_name + ".png"


def captureScreenWithCaseAndStep(test_case: str, test_step: str) -> str:
    return captureScreen(test_case+"_"+test_step)


def runOverlay():
    return runProgram(WIN_CONST['AHK_EXE'] + ' ' + WIN_CONST['AHK_BASE'] + '\\Windows_Overlay.ahk')