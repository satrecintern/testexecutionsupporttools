import csv

class ProjectTestCase:
    def __init__(self, project_name:str, platform:str, tp_name:str, tp_version:str, tc_name:str, tc_des:str, tc_precon:str, step_id:int, step_des:str, expected_result_info:str, expected_result:str,first_result:str, actual_result:str):
        self._projectName = project_name
        self._platform = platform # platform : web or desktop
        self._tpName = tp_name
        self._tpVersion = tp_version
        self._tcName = tc_name
        self._tcDes = tc_des
        self._tcPrecon = tc_precon
        self._stepId = step_id
        self._stepDes = step_des
        self._expectedResultInfo = expected_result_info # 예상 결과
        self._expectedResult = expected_result # 예상 결과 입력값
        self._firstResult = first_result # 초기수행 결과 변수
        self._actualResult = actual_result

def parsing(f):
    num = 0
    tc_instance_list = []

    rdr = csv.DictReader(f)
    for i in rdr:
        if (i['시험 절차 스텝 번호'] == '1'):
            if(num != 0):
                tc = ProjectTestCase(project_name, platform, tp_name, tp_version, tc_name, tc_des, tc_precon, step_id, step_info, expected_result_info, expected_result, first_result, actual_result)
                tc_instance_list.append(tc)
            step_id = []
            step_info = []
            expected_result_info = []
            expected_result = []
            first_result = []
            actual_result = []
            project_name = i['프로젝트명']
            platform = i['플랫폼']
            tp_name = i['테스트 프로시저 이름']
            tp_version = i['테스트 프로시저 버전']
            tc_name = i['테스트 케이스 이름']
            tc_des = i['테스트 케이스 설명']
            tc_precon = i['테스트 사전 조건']
            step_id.append(int(i['시험 절차 스텝 번호']))
            step_info.append(i['시험 절차 설명'])
            expected_result_info.append(i['예상 결과'])
            expected_result.append(i['예상 결과 입력값'])
            first_result.append(i['초기 수행 결과'])
            actual_result.append(i['실제 결과'])
        else:
            step_id.append(int(i['시험 절차 스텝 번호']))
            step_info.append(i['시험 절차 설명'])
            expected_result_info.append(i['예상 결과'])
            expected_result.append(i['예상 결과 입력값'])
            first_result.append(i['초기 수행 결과'])
            actual_result.append(i['실제 결과'])
        num = num+1
    tc = ProjectTestCase(project_name, platform, tp_name, tp_version, tc_name, tc_des, tc_precon, step_id, step_info, expected_result_info, expected_result, first_result, actual_result)
    tc_instance_list.append(tc)

    return tc_instance_list